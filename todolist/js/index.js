// select elements
const toDoList = document.getElementById("addItem");
const todoInput = document.getElementById("newTask");
const todosListEl = document.getElementById("todo");
const completedtodosListEl = document.getElementById("completed");

//vars
let todos = JSON.parse(localStorage.getItem('todos'))  || [];
let EditTodoId = -1;

//1st render 
renderTodos();

//form submit
toDoList.addEventListener("click", function (event) {
  event.preventDefault();

  saveTodo();
  renderTodos();
  localStorage.setItem("todos", JSON.stringify(todos))
});

//save to do
function saveTodo() {
  const todoValue = todoInput.value;

  //check if the todo is empty
  const isEmpty = todoValue === "";

  //check for duplicate todos
  const isDuplicate = todos.some(
    (todo) => todo.value.toUpperCase() === todoValue.toUpperCase()
  );

  if (isEmpty) {
    alert("Gòi sao ko điền chữ? Điền zô đi kìa");
  } else if (isDuplicate) {
    alert("Todo đã tồn tại.");
  } else {
    if(EditTodoId >= 0){
        todos = todos.map((todo, index) =>({
            
                ...todo,
                value: index === EditTodoId ? todoValue : todo.value,
            

        }));
        EditTodoId -1;
        //update the edit todo

    }else {
        todos.push({
            value: todoValue,
            checked: false,
            color: "#" + Math.floor(Math.random() * 16777215).toString(16),
          });
    }
   

    todoInput.value = "";
  }
}

//render todos
function renderTodos() {
    if(todos.length === 0){
        todosListEl.innerHTML = `<center>You have nothing to-do!</center>`
        return;
    }
  //clear element before a-rểnder

  todosListEl.innerHTML = "";
    let hasChecked = false;
  //render todos

  
  todos.forEach((todo, index) => {
    if (!hasChecked && todo.checked) {
        hasChecked = true;
    }
    todosListEl.innerHTML += `

<li class="todo"
 id=${index}>
    <i class="bi ${todo.checked ? 'bi-check-circle-fill' : 'bi-circle'}"
    style="color : ${todo.color}"
    data-action="check"
    ></i> 
    <p class="${todo.checked ? 'checked' : ''}"
    data-action="check">${todo.value}</p>
    <i class="bi bi-pencil-square"
    style="color: blue;
    
    transform: translateX(120px)"
    data-action="edit">

    </i>
    <i class="bi bi-trash"
    style="color: red"
    data-action="delete">
    </i>
</li>

`;
  });
  
  if (hasChecked) {
    // completedtodosListEl.innerHTML = `<p>Checked</p>`;
    toDoList.setAttribute('data-value', 'checked' );
    completedtodosListEl.setAttribute('data-value', '');
    
  } else {
    // completedtodosListEl.innerHTML = `<p>Unchecked</p>`;
    toDoList.setAttribute('data-value', '' );
    completedtodosListEl.setAttribute('data-value', 'You have yet to complete any tasks.' )
  
}
  console.log('test: ' + hasChecked);
}

//click event listener for all the todoes

todosListEl.addEventListener("click", (event) => {
  const target = event.target;
  const parentElement = target.parentNode;

  if (parentElement.className !== "todo") return;

  const todo = parentElement;
  const todoId = Number(todo.id);

//target action
 const action = target.dataset.action

action === 'check' && checkTodo(todoId)
action === "edit" && editTodo(todoId)
action === "delete" && deleteTodo(todoId)
});


//Check a todo
function checkTodo(todoId){
    todos = todos.map((todo, index) => 
({
                ...todo, //spread operator
                checked: index === todoId ? !todo.checked : todo.checked,
            }));

            renderTodos();
            localStorage.setItem("todos", JSON.stringify(todos))

}

//edit a todo
function editTodo(todoId) {
    todoInput.value = todos[todoId].value;
    EditTodoId = todoId;

}

//delete a todo
function deleteTodo(todoId) {
    todos = todos.filter((todo, index) => index !== todoId);
    EditTodoId = -1;

    //re-render
    renderTodos();
    localStorage.setItem("todos", JSON.stringify(todos))

}

//sort value A->Z
function SortAZ() {
    if (todos.length) {
        todos.sort((a,b) => a['value'].localeCompare(b['value']));
        renderTodos();
    }
    console.log(todos);

}

//sort value Z-->A
function SortZA(){
    if (todos.length) {
        todos.sort((a,b) => b['value'].localeCompare(a['value']));
        renderTodos();
    }
}